﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumbersPairs
{
    class Program
    {
        static void Main(string[] args)
        {
            int pairSum = 5;
            int arraySize = 20;

            // Filling array
            int[] numbers = new int[arraySize];

            Random rnd = new Random(DateTime.Now.Millisecond);

            Console.WriteLine("Source array: ");
            for (int i = 0; i < arraySize; i++)
            {
                numbers[i] = rnd.Next(0, 10); // Taking random numbers from 0 to 9
                Console.Write(numbers[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Pair sum: " + pairSum);
            Console.WriteLine();

            // Realization
            int[] pairs = new int[arraySize];
            for (int i = 0; i < arraySize; i++) pairs[i] = -1;

            Console.WriteLine("Pairs: ");

            for (int i = 0; i < arraySize - 1; i++)
            {
                if (pairs[i] == -1)
                {
                    for (int j = i + 1; j < arraySize; j++)
                    {
                        if (numbers[i] + numbers[j] == pairSum && pairs[j] == -1)
                        {
                            pairs[j] = i;
                            Console.Write(string.Format("({0}, {1}) ", numbers[i], numbers[j]));

                            break;
                        }
                    }
                }               
            }

            Console.ReadKey();
        }
    }
}
