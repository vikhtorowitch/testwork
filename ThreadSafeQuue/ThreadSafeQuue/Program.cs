﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadSafeQueue
{
    class ThreadSafeQueue<T>
    {        
        private Queue _queue;

        private Mutex _mutex;
        private object lockObject = new object();

        public ThreadSafeQueue()
        {
            _queue = new Queue();
            _mutex = new Mutex();
        }

        public void Push(T item)
        {
            lock (lockObject)
            {
                _queue.Enqueue(item);
            }
        }

        public T Pop()
        {
            _mutex.WaitOne();
            while (_queue.Count == 0) Thread.Sleep(20);

            object result;
            lock (lockObject)
            {
                result = _queue.Dequeue();
            }

            _mutex.ReleaseMutex();
            return (T)result;
        }
    }

    class WritingThread
    {
        private Thread _thread;

        private ThreadSafeQueue<string> _queue;

        public WritingThread(ThreadSafeQueue<string> queue)
        {
            _queue = queue;
            _thread = new Thread(Work);
            _thread.Start();
        }

        private void Work()
        {
            var rnd = new Random(687354687);
            int loopsCount = rnd.Next(50, 200);

            for (var i = 0; i < loopsCount; i++)
            {
                var str = "string" + i.ToString();
                _queue.Push(str);
                Console.WriteLine("Pushed " + str);
                //Thread.Sleep(rnd.Next(100, 1000));
            }
        }
    }

    class ReadingThread
    {
        private Thread _thread;

        private ThreadSafeQueue<string> _queue;

        public ReadingThread(ThreadSafeQueue<string> queue)
        {
            _queue = queue;
            _thread = new Thread(Work);
            _thread.Start();
        }

        private void Work()
        {
            var rnd = new Random(687354687);
            int loopsCount = rnd.Next(50, 200);

            for (var i = 0; i < loopsCount; i++)
            {
                var str =  _queue.Pop();
                Console.WriteLine("Poped " + str);
                //Thread.Sleep(rnd.Next(100, 1000));
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var queue = new ThreadSafeQueue<string>();

            for (int i = 0; i < 50; i++)
            {
                var wt = new ReadingThread(queue);
                var rt = new WritingThread(queue);
            }
        }
    }
}
